This is a python 3 microservice returning the answer to a medical question.

Create the database
===================

    echo 'create table chatbot(input text, output text);' | sqlite3 chattybot.db

Run the app
===========

    pip install -r requirements.txt
    export FLASK_APP=app.py
    flask run

Start integration tests
=======================

    export CHATTYBOT_ENDPOINT=http://localhost:5000
    python -m unittest

Build docker container
=======================
    docker build -t chattybot .
    
Run docker container
=======================

    docker run -it --rm --name chattybot chattybot


Start integration tests for dockerised (Linux systems)
======================================================

    docker inspect chattybot|grep IPAddress # in order to get the IP
    export CHATTYBOT_ENDPOINT=<IPAddress found> 
    python -m unittest



    
