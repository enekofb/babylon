from flask import Flask
from flask import request
import os
from flask_sqlalchemy import SQLAlchemy


app = Flask(__name__)
logger = app.logger
app.config['SQLALCHEMY_DATABASE_URI'] = os.getenv('DATASOURCE_URL')
db = SQLAlchemy(app)

class ChatBot(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    input = db.Column(db.String(80), unique=False, nullable=False)
    output = db.Column(db.String(120), unique=False, nullable=False)

db.create_all()
conn = db.engine.connect()

@app.route('/')
def hello():
    logger.info('request received for hello endpoint')

    return 'Hello! Use the /input route, e.g. /input?message={patient_message}'


@app.route('/health')
def health():
    logger.info('request received for health endpoint')
    return 'OK'


@app.route('/backdoor')
def secret():
    logger.info('request received for backdoor')
    all_chats = []
    for chat in conn.execute('select * from chat_bot'):
        all_chats.append(chat)
    return str(all_chats)


def escape(text):
    return text.replace("'","''")


@app.route('/input')
def answer():
    text_in = request.args.get('message')
    logger.info('request received for input with {}'.format(text_in))
    text_out = diagnosis(text_in)
    query = 'insert into chat_bot(input, output) values (\'{}\',\'{}\')'.format(escape(text_in),escape(text_out));
    with db.engine.connect() as conn:
        conn.execute(query)
    return '{"diagnosis": "' + text_out + '"}'


# TODO: I am a service .. .refactor me!
def diagnosis(symptom):
    # symptoms

    HELLO_SYMPTOM = 'hello!'
    HI_SYMPTOM = 'hi'
    WHATSUP_SYMPTOM = "what's up"
    HEAD_HURT_SYMPTON = 'my head hurts'

    # illnesses deteced

    GENERAL_ILL_DETECTED = 'Hello my good friend! You look ill.'
    MIGRAINE_DETECTED = "It's probably a migraine"

    DEFAULT_ILLNESS_DETECTED = "It's not lupus."

    diagnostic = {
        HELLO_SYMPTOM: GENERAL_ILL_DETECTED,
        HI_SYMPTOM: GENERAL_ILL_DETECTED,
        WHATSUP_SYMPTOM: GENERAL_ILL_DETECTED,
        HEAD_HURT_SYMPTON: MIGRAINE_DETECTED
    }
    return diagnostic.get(symptom, DEFAULT_ILLNESS_DETECTED)


if __name__ == '__main__':
    app.run(host='0.0.0.0',debug=True)
