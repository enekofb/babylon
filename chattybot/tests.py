#!/usr/bin/env python
import requests
import os
import unittest

endpoint = os.getenv("CHATTYBOT_ENDPOINT")


class BaseEndpoint(unittest.TestCase):
    def test(self):
        response = requests.get(endpoint + '/')
        self.assertEqual(response.status_code, 200)


class HealthEndpoint(unittest.TestCase):
    def test(self):
        response = requests.get(endpoint + '/health')
        self.assertEqual(response.status_code, 200)


def detectAndAssertIllness(self,patient_message,illness_message):
    speak_endpoint_url='{}/input?message={}'.format(endpoint,patient_message)
    response = requests.get(speak_endpoint_url)
    response_body = response.json()
    self.assertEqual(response.status_code, 200)
    self.assertEqual(response_body['diagnosis'], illness_message)

class SpeakEndpoint(unittest.TestCase):


    def testShouldReturnGeneralIllWhenHello(self):
        patient_message ='hello!'
        general_ill_message = 'Hello my good friend! You look ill.'
        detectAndAssertIllness(self,patient_message,general_ill_message)

    def testShouldReturnGeneralIllWhenHi(self):
        patient_message ='hi'
        general_ill_message = 'Hello my good friend! You look ill.'
        detectAndAssertIllness(self,patient_message,general_ill_message)

    def testShouldReturnGeneralIllWhenWhatsup(self):
        patient_message ="what's up"
        general_ill_message = 'Hello my good friend! You look ill.'
        detectAndAssertIllness(self,patient_message,general_ill_message)

    def testShouldReturnMigraineWhenHeadHurts(self):
        patient_message ="my head hurts"
        general_ill_message = "It's probably a migraine"
        detectAndAssertIllness(self,patient_message,general_ill_message)

    def testShouldReturnNotLumpusOtherwise(self):
        patient_message = 'My back is painful'
        general_ill_message = "It's not lupus."
        detectAndAssertIllness(self,patient_message,general_ill_message)