#!/usr/bin/env bash

cd $CHATTYBOT_HOME

export CHATTYBOT_ENDPOINT=http://`minikube ip`:30001

python -m unittest