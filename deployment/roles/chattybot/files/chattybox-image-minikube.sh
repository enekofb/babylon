#!/usr/bin/env bash

eval $(minikube docker-env)

docker build -t chattybot $CHATTYBOT_HOME

docker images|grep chattybot

