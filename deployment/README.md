# Deploy chattybot into kubernetes

## Requirements

1) Setup the following environment variables

        #home directory where the app codes lives
        export CHATTYBOT_HOME=/Users/eneko/projects/enekofb/babylon/chattybot 


## Deploying Chattybot

An ansible playbook has been provided for an easy provisioning of previous dependencies

*provision-kubernetes-local.yaml playbook*

Playbook that setups the environment for running chattybot. It does the following.

    - Installs kubectl locally
    - Installs minikube locally
    - Installs Helm in minikube
    - Verify you have installed docker 


To execute the playbook just ... `ansible-playbook provision-kubernetes-local.yaml`.

**Note** that the playbook installs against `/usr/local/bin` so make sure that you want to do that
before executing the playbook.


*deploy-chattybot-kubernetes.yaml playbook*

Playbook that does the following

    - Installs mysql in kubernetes using helm
    - Installs postgresql in kubenrets using helm
    - Builds chattybot docker image using minikube docker daemon
    - Deploys chattybot applicatino into minikube
    - Run Tests against deployed application


To execute the playbook just ... `ansible-playbook deploy-chattybot-kubernetes.yaml`.

 

    