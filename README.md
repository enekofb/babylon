Intro
=====

Welcome to the babylon devops tech test!

You will be working on the prototype for our next-generation secure medical-grade chatbot.

This project has been started by one of our best interns, and we expect to deploy it across the world in Q5 2020.

Tasks
=====

Fix bugs
--------

Several bugs currently roam in the code. Please fix them as you go along to make the tests pass.

*Solution Considerations*

1) renaming methods in order to avoid clashing "hello" was not unique
2) escaping different strings


Add new functionality
---------------------

The chatbot currently only reacts in a fairly limited fashion.

Make it react to the following:

- [FEATURE/DETECT-GENERAL-ILL] "hello!", "hi" and "what's up" should return "Hello my good friend! You look ill."
- [FEATURE/DETECT-MIGRAINE] "my head hurts" should return "It's probably a migraine"
- [FEATURE/NOT-ILLNESS-DETECTED] Any other questions should return "It's not lupus."

*Solution Considerations*

- Design: refactored method _input_ to accept the message by _query param_ over _path param_. It feels more suitable
so in a Rest world path params are used for navigation between resources.

- DB: refactored connection and insertion query.

- Implementation: 

- TDD approach to implement _illness_detector_

- [FEATURE/DETECT-GENERAL-ILL] "hello!", "hi" and "what's up" should return "Hello my good friend! You look ill."
- [FEATURE/DETECT-MIGRAINE] "my head hurts" should return "It's probably a migraine"
- [FEATURE/NOT-ILLNESS-DETECTED] Any other questions should return "It's not lupus."


Dockerisation
-------------

The microservice currently only runs as a python script.

You should dockerise it with the aim of:

- Secure application runtime
- Small container size
- Legible & maintainable Dockerfile

Feel free to use any official base image from the Docker Hub as a base (e.g. `alpine`, `python`...).

*Solution Considerations*

- Dockerfile created based in alpine. Docker found [here](./chattybot/Dockerfile).
- Deployment folder created in order to deploy and test the application through containers. 
- Using kubernetes (minikube) as a deployed environment instead of docker-compose.
- Created ansible playbooks for both 1) provisioning a minikube environment and to deploy/test chattybot
- Using helm for easy deployment
- Playbooks

    provision-kubernetes-local.yaml --> provision kubectl, minikube and helm in local
    deploy-chattybot-kubernetes.yaml --> deploy chattybot in kube as deployment, expose it a service/nodeport and run test suite
    

*Limitations in the solution*

- How chattybot is exposed in kubernetes. Exposing chattybot service as NodePort in order to test from outside the cluster. It should be ideally done
by using ingress and ingress controller. I have added the tasks using helm and nginx-ingress controller chart that would 
address this situation but just as an indication.

- How we build and push chattybot docker image. Also for simplicity I have point my local docker client to docker daemon running in minikube in order
to build the chattybot docker image. A proper solution should be by using docker registry. A docker registry ansible using helm
has been also added to the project just for indication but for simplicity I have prefered to go in this direction. 

- Local vs Virtual Environment: the solution should be better deployed using a virtualised environment (by Vagrant for instance) so then there is no need of installing minikube and so in the local machine.


Build docker container
=======================
    dokcer build -t chattybot .
    
Run docker container
=======================

    docker run -it --rm --name chattybot chattybot


Persistence
-----------

*The microservice currently uses a SQLite database as a backend. Explain the tradeoffs inherent to that choice.*

Is just not feasible solution for a production service: 

1- It is coupled to the service so their lifecycle is coupled and cannot scale idependentily.
2- Its storage is  single-file based so in terms of db-performance is a constraint.
3- It is not HA so represents a single point of failure.


*For extra credit, switch this to a more robust SQL database, either PostgreSQL or MySQL, using docker-compose.*

Instead of using docker compose we keep using our solution based on kubernetes/helm

1) Modified the app for using SQLAlchemy for managing the database through `DATASOURCE_URL` environment variable

    app.config['SQLALCHEMY_DATABASE_URI'] = os.getenv('DATASOURCE_URL')
    db = SQLAlchemy(app)
    
    class ChatBot(db.Model):
        id = db.Column(db.Integer, primary_key=True)
        input = db.Column(db.String(80), unique=False, nullable=False)
        output = db.Column(db.String(120), unique=False, nullable=False)

2) Modified kubernetes resource list

- Added a configmap `chattybot-confg` with externalised configuration for the app. It defines
the database-url configuration property

- Modified chattybot deployment for setting up the previous property as environment variable in the container.

            - image: chattybot
              name: chattybot
              imagePullPolicy: IfNotPresent
              ports:
                - containerPort: 5000
              env:
              - name: DATASOURCE_URL
                valueFrom:
                  configMapKeyRef:
                    key: environment
                    name: database-url

*Bonus points for using parameters to switch between database endpoints.*

- In order to switch between both of them you just need to setup the proper configuration value, either mysql-url or postgresql-url

                      env:
                      - name: DATASOURCE_URL
                        valueFrom:
                          configMapKeyRef:
                            name: chattybot-config
                            key: mysql-url
            
            - apiVersion: v1
              kind: ConfigMap
              metadata:
                name: chattybot-config
              data:
                postgresql-url: postgresql://chattybot:chattybotchattybot@postgresql-postgresql:5432/chattybot
                mysql-url: mysql+pymysql://chattybot:chattybotchattybot@mysql-mysql:3306/chattybot

Improvements and future considerations
--------------------------------------

*Point out any issues you currently see in the app, and how you would fix them.*

- The business logic is pretty simple and static.
- There is no caching while the solutions are static.
- There is no need of having backends, the previous logic could be easily implemented by using FaaS.
- No security implemented. Authorization mechanism to implement. 

*Also, what architectural ways would you explore to scale this service:*

- to 1000 requests per second?

1) Use caching so the questions/answers are static.
2) Scale horizontally the service. Based on the solution given, by having as many replicas of the service as you 
need based on capacity planning. Putting a loadbalancer in front of them.
3) Moving to FaaS

Example of our service horizontally scaled

```
- apiVersion: extensions/v1beta1
  kind: Deployment
  metadata:
    labels:
      app: chattybot
    name: chattybot
  spec:
    replicas: 100

``` 
- to million of users?

1) Use of a caching service that could be also per region.

2) Iterating the previous solution or trying to partition the requests by regions. Using the dns per region
pointing to a cluster that provides service to that region.  

EUROPE -> DNS -> LB EU -> Cluster EU -> X replicas of the service
AMERICA -> DNS -> LB AMERICA -> Cluster AMERICA -> Y replicas of the service
ASIA -> DNS -> LB ASIA -> Cluster ASIA -> Z replicas of the service

3) Moving to FaaS

- to several regions across the globe?

Same solution as for previous one

General questions
=================

These are just small questions; a one-line answer should be enough. When applicable, feel free to simply use a bash command line as an answer.

Unless specified, assume a modern linux, e.g. CentOS 7.

Don't worry about open-ended questions - some have no definitive "correct" answers.

- On a linux server, how would you search for the string "Error 500" in a logfile located at `/var/log/example.log`?

        grep "Error 500" /var/log/example.log

- What about all logs in the `/var/log/` folder?

        grep "Error 500" /var/log/*

- Recursively?

        grep -r "Error 500" /var/log
 
- What's the main difference between Subversion and Git?

        They are both SCM system that while Subversion is centralised, it means there is just one remote server and the operations 
        are done against it, Git is distributed so you have a local one that has independent lifycle that the remote that could be one or more.

- Which mechanism would you use to run a task every monday at 5am on a machine?

        I would use cron or any other task scheduler. 0 5 * * 1 should be the cron expression.

- What about running a long-lived process?

        You might want to use any service management in unix as could be systemd or init.d. Also you could use containers. 

- You overhear: "My web service works fine when connecting on localhost, but not remotely". What would be the most common causes?

        Different situations can cause this: 
        
        1) The service is not listening in the interface that is connected to internet or public face.
        2) There is no public access to the remote server configured (interfaces, routing, dns). 
 

- What's the difference between the following commands:

* `cmd1; cmd2` -> executes cmd1 then cmd2 in every case.
* `cmd1 && cmd2` -> executes cmd2 after cmd1 only if cmd1 has been executed sucessfully (exit code == 0)
* `cmd1 & cmd2` -> cmd1 is executed in the background and cmd2 foreground.

- On a problematic box, how would you diagnose high CPU load? High memory load? High disk usage? High I/O load?

        - top for memory and cpu usage
        - df for disk usage
        - iostat for i/o usage


- Is a t2 instance on AWS suitable for production workloads?

        As a rule of thumbs, no so it stands for Tiny and you generally dont want to have this small machines running your production 
        traffic. Said that, it depends on the service that you are running and the most important the type of the traffic and processing
        it is doing. You should use the most efficient type of instance that suits you.   

- You need to store large amounts of data in a cost-effective fashion on the cloud. Which service would you use?

        Depends on different elements about your data:
         
        1) Type of the data to storage whether it is block, object. 
        2) Availablity and error tolerance.
        3) Time to retrieval and access latency. 
        
        said that, in AWS, for object storage, S3 offer different modes S3, S3 RR, S3 IA and Glacier that as a suite with 
        lifecycle policies enables you to have your data stored in a cost/efficient manner.

- You have two EC2 instances in different VPCs. How do you make them communicate?

        VPC peering would be the simplest way of doing it. There are also other ways like using ElasticIps/Public Ips or through Bastion Boxes.

- How do you get the first 100 bytes of an executable?
        
        hexdump -n 100 <binary_path>

- How do you change the time a file has been modified?
        
        touch -m <file_path>
